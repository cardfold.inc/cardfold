﻿using System.Threading.Tasks;
using Cardfold.Domain.Models;
using Cardfold.Services.DTOs;
using Cardfold.Services.Interfaces;

namespace Cardfold.Services.Impls
{
    public class ColumnService<TId> : IColumnService<TId>
    {
        public async Task<Column<TId>> Create(BasicColumnModel<TId> columnModel)
        {
            throw new System.NotImplementedException();
        }

        public async Task<Column<TId>> Get(TId columnId)
        {
            throw new System.NotImplementedException();
        }

        public async Task<Column<TId>> Update(BasicColumnModel<TId> updateModel)
        {
            throw new System.NotImplementedException();
        }

        public async Task Delete(TId columnId)
        {
            throw new System.NotImplementedException();
        }
    }
}
