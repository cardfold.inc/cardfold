﻿using System.Threading.Tasks;
using Cardfold.Domain.Models;
using Cardfold.Services.DTOs;
using Cardfold.Services.Interfaces;

namespace Cardfold.Services.Impls
{
    public class CardService<TId> : ICardService<TId>
    {
        public async Task<Card<TId>> Create(BasicCardModel<TId> cardModel)
        {
            throw new System.NotImplementedException();
        }

        public async Task<Card<TId>> Get(TId cardId)
        {
            throw new System.NotImplementedException();
        }

        public async Task<Card<TId>> Update(BasicCardModel<TId> updateModel)
        {
            throw new System.NotImplementedException();
        }

        public async Task Delete(TId cardId)
        {
            throw new System.NotImplementedException();
        }
    }
}
