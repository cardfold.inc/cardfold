﻿using System;
using System.Threading.Tasks;
using Cardfold.Domain.Models;
using Cardfold.Repository.Models;
using Cardfold.Services.DTOs;
using Cardfold.Services.Interfaces;

namespace Cardfold.Services.Impls
{
    public class CommentService<TId> : ICommentService<TId>
    {
        public async Task<Comment<TId>> Create(BasicCommentModel<TId> commentModel)
        {
            throw new NotImplementedException();
        }

        public async Task<Comment<TId>> Update(BasicCommentModel<TId> updatedModel)
        {
            throw new NotImplementedException();
        }

        public async Task Delete(TId commentId)
        {
            throw new NotImplementedException();
        }
    }
}
