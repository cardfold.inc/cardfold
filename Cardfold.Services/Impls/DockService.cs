﻿using System.Threading.Tasks;
using Cardfold.Domain.Models;
using Cardfold.Services.DTOs;
using Cardfold.Services.Interfaces;

namespace Cardfold.Services.Impls
{
    public class DockService<TId> : IDockService<TId>
    {
        public async Task<Dock<TId>> Create(BasicDockModel<TId> basicDockModel)
        {
            throw new System.NotImplementedException();
        }

        public async Task<Dock<TId>> Get(TId dockId)
        {
            throw new System.NotImplementedException();
        }

        public async Task<Dock<TId>> Update(BasicDockModel<TId> updateModel)
        {
            throw new System.NotImplementedException();
        }

        public async Task Delete(TId dockId)
        {
            throw new System.NotImplementedException();
        }
    }
}
