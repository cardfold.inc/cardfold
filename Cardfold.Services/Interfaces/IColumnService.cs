﻿using System.Threading.Tasks;
using Cardfold.Domain.Models;
using Cardfold.Services.DTOs;

namespace Cardfold.Services.Interfaces
{
    public interface IColumnService<TId>
    {
        Task<Column<TId>> Create(BasicColumnModel<TId> columnModel);

        Task<Column<TId>> Get(TId columnModel);

        Task<Column<TId>> Update(BasicColumnModel<TId> updateModel);

        Task Delete(TId columnModel);
    }
}
