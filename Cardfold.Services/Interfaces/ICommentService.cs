﻿using System.Threading.Tasks;
using Cardfold.Domain.Models;
using Cardfold.Services.DTOs;

namespace Cardfold.Services.Interfaces
{
    public interface ICommentService<TId>
    {
        Task<Comment<TId>> Create(BasicCommentModel<TId> commentModel);

        Task<Comment<TId>> Update(BasicCommentModel<TId> updatedModel);

        Task Delete(TId commentId);
    }
}
