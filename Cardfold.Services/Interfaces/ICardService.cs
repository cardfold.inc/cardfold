﻿using System.Threading.Tasks;
using Cardfold.Domain.Models;
using Cardfold.Services.DTOs;

namespace Cardfold.Services.Interfaces
{
    public interface ICardService<TId>
    {
        Task<Card<TId>> Create(BasicCardModel<TId> cardModel);

        Task<Card<TId>> Get(TId cardId);

        Task<Card<TId>> Update(BasicCardModel<TId> sourceModel);

        Task Delete(TId cardId);
    }
}
