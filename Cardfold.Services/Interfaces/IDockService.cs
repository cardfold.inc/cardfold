﻿using System.Threading.Tasks;
using Cardfold.Domain.Models;
using Cardfold.Services.DTOs;

namespace Cardfold.Services.Interfaces
{
    public interface IDockService<TId>
    {
        Task<Dock<TId>> Create(BasicDockModel<TId> basicDockModel);

        Task<Dock<TId>> Get(TId dockId);

        Task<Dock<TId>> Update(BasicDockModel<TId> updateModel);

        Task Delete(TId dockId);
    }
}
