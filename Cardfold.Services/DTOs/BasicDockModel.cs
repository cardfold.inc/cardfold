﻿namespace Cardfold.Services.DTOs
{
    public class BasicDockModel<TId>
    {
        public TId Id { get; set; }

        public string TeamId { get; set; }
        
        public string Name { get; set; }
        
        public string Description { get; set; }
    }
}
