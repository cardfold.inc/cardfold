﻿namespace Cardfold.Services.DTOs
{
    public class BasicColumnModel<TId>
    {
        public TId Id { get; set; }

        public string TeamId { get; set; }

        public string DockName { get; set; }

        public string ColumnName { get; set; }
    }
}
