﻿namespace Cardfold.Services.DTOs
{
    public class BasicCardModel<TId>
    {
        public TId Id { get; set; }

        public string CardName { get; set; }

        public string Description { get; set; }

        public string Color { get; set; }
    }
}
