﻿using System.Collections.Generic;

namespace Cardfold.Services.DTOs
{
    public class BasicCommentModel<TId>
    {
        public TId Id { get; set; }

        public string DockName { get; set; }

        public string ColumnName { get; set; }

        public string CardName { get; set; }

        public IEnumerable<string> ParentCommentIds { get; set; }

        public string UserId { get; set; }

        public string Text { get; set; }
    }
}
