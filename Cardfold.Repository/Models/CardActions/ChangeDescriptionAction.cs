﻿using System;
using Cardfold.Domain.Models.CardActions;

namespace Cardfold.Repository.Models.CardActions
{
    public class ChangeDescriptionAction<TId> : ICardAction<TId>
    {
        public TId ActorId { get; set; }

        public DateTime CreatedAt { get; set; }

        public string FromDescription { get; set; }

        public string ToDescription { get; set; }
    }
}
