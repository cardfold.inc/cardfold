﻿using Cardfold.Domain.Models.CardActions;
using System;

namespace Cardfold.Repository.Models.CardActions
{
    public class CreateAction<TId> : ICardAction<TId>
    {
        public TId ActorId { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
