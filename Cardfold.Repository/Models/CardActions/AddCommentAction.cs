﻿
using System;
using Cardfold.Domain.Models.CardActions;

namespace Cardfold.Repository.Models.CardActions
{
    public class AddCommentAction<TId> : ICardAction<TId>
    {
        public TId ActorId { get; set; }

        public DateTime CreatedAt { get; set; }

        public TId CommentId { get; set; }
    }
}
