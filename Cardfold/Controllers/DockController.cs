﻿using System.Threading.Tasks;
using Cardfold.Services.DTOs;
using Cardfold.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Cardfold.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DockController<TId> : ControllerBase
    {
        private readonly IDockService<TId> _dockService;

        public DockController(IDockService<TId> dockService)
        {
            _dockService = dockService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateDock(BasicDockModel<TId> dockModel)
        {
            return Ok(await _dockService.Create(dockModel));
        }

        [HttpGet]
        public async Task<IActionResult> GetDock(TId dockId)
        {
            return Ok(await _dockService.Get(dockId));
        }

        [HttpPut]
        public async Task<IActionResult> Update(BasicDockModel<TId> updateModel)
        {
            return Ok(await _dockService.Update(updateModel));
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteDock(TId dockId)
        {
            await _dockService.Delete(dockId);
            return NoContent();
        }
    }
}
