﻿using System.Threading.Tasks;
using Cardfold.Services.DTOs;
using Cardfold.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Cardfold.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ColumnController<TId> : ControllerBase
    {
        private readonly IColumnService<TId> _columnService;

        public ColumnController(IColumnService<TId> columnService)
        {
            _columnService = columnService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateColumn(BasicColumnModel<TId> columnModel)
        {
            return Ok(await _columnService.Create(columnModel));
        }

        [HttpGet]
        public async Task<IActionResult> GetColumn(TId columnId)
        {
            return Ok(await _columnService.Get(columnId));
        }

        [HttpPut]
        public async Task<IActionResult> Update(BasicColumnModel<TId> updateModel)
        {
            return Ok(await _columnService.Update(updateModel));
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteColumn(TId columnName)
        {
            await _columnService.Delete(columnName);
            return NoContent();
        }
    }
}