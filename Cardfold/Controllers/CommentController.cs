﻿using System.Threading.Tasks;
using Cardfold.Services.DTOs;
using Cardfold.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Cardfold.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController<TId> : ControllerBase
    {
        private readonly ICommentService<TId> _commentService;

        public CommentController(ICommentService<TId> commentService)
        {
            _commentService = commentService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateComment(BasicCommentModel<TId> commentModel)
        {
            return Ok(await _commentService.Create(commentModel));
        }

        [HttpPut]
        public async Task<IActionResult> Update(BasicCommentModel<TId> updateModel)
        {
            return Ok(await _commentService.Update(updateModel));
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteComment(TId commentId)
        {
            await _commentService.Delete(commentId);
            return NoContent();
        }
    }
}
