﻿using System.Threading.Tasks;
using Cardfold.Services.DTOs;
using Cardfold.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Cardfold.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CardController<TId> : ControllerBase
    {
        private readonly ICardService<TId> _cardService;

        public CardController(ICardService<TId> cardService)
        {
            _cardService = cardService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateCard(BasicCardModel<TId> cardModel)
        {
            return Ok(await _cardService.Create(cardModel));
        }

        [HttpGet]
        public async Task<IActionResult> GetCard(TId cardId)
        {
            return Ok(await _cardService.Get(cardId));
        }

        [HttpPut]
        public async Task<IActionResult> Update(BasicCardModel<TId> updateModel)
        {
            return Ok(await _cardService.Update(updateModel));
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteCard(TId cardId)
        {
            await _cardService.Delete(cardId);
            return NoContent();
        }
    }
}
