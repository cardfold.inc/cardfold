﻿using Cardfold.Services.Impls;
using Cardfold.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Cardfold.Root
{
    public static class CompositionRoot<TId>
    {
        public static void SetupDependencies(IServiceCollection services)
        {
            services.AddSingleton<IDockService<TId>, DockService<TId>>();
            services.AddSingleton<IColumnService<TId>, ColumnService<TId>>();
            services.AddSingleton<ICardService<TId>, CardService<TId>>();
            services.AddSingleton<ICommentService<TId>, CommentService<TId>>();
        }
    }
}
