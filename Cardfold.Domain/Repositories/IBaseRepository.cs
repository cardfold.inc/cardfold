﻿using Cardfold.Domain.Models;

namespace Cardfold.Domain.Repositories
{
    public interface IBaseRepository<TId, TObject>
    {
        IOutput<TObject> Get(TId id);

        IOutput<TObject> Create(TObject updateObject);
       
        IOutput<TObject> Update(TObject updateObject);

        IOutput<TObject> Delete(TId updateObject);
    }
}
