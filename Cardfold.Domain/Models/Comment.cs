﻿using System.Collections.Generic;

namespace Cardfold.Domain.Models
{
    public class Comment<TId>
    {
        public TId Id { get; set; }

        public string UserId { get; set; }

        public string Text { get; set; }

        public IEnumerable<Comment<TId>> Replies { get; set; }
    }
}
