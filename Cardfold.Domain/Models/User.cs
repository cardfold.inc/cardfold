﻿
namespace Cardfold.Domain.Models
{
    public class User<TId>
    {
        public TId Id { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }
    }
}
