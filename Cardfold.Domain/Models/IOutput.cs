﻿namespace Cardfold.Domain.Models
{
    public interface IOutput<TResult>
    {
        bool IsSucces { get; set; }
        TResult Result { get; set; }
    }

    public class Output<TResult> : IOutput<TResult>
    {
        public bool IsSucces { get; set; }
        public TResult Result { get; set; }
    }
}
