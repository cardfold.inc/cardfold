﻿using System;

namespace Cardfold.Domain.Models.CardActions
{
    public interface ICardAction<TId>
    {
        TId ActorId { get; set; }

        DateTime CreatedAt { get; set; }
    }
}
