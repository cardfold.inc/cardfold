﻿using System.Collections.Generic;

namespace Cardfold.Domain.Models
{
    public class Dock<TId>
    {
        public TId TeamId { get; set; }

        public TId Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public IEnumerable<Column<TId>> Columns { get; set; }
    }
}
