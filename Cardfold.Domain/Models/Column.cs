﻿using System.Collections.Generic;

namespace Cardfold.Domain.Models
{
    public class Column<TId>
    {
        public TId Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<Card<TId>> Cards { get; set; }
    }
}
