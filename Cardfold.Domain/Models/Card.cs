﻿using System.Collections.Generic;
using Cardfold.Domain.Models.CardActions;

namespace Cardfold.Domain.Models
{
    public class Card<TId>
    {
        public TId Id { get; set; }

        public User<TId> AssignedTo { get; set; }

        public IEnumerable<ICardAction<TId>> CardActions { get;set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Color { get; set; }

        public IEnumerable<Comment<TId>> Comments { get; set; }
    }
}
